import ShiftTime, {
  HourOutOfRangeError,
  MinuteOutOfRangeError,
} from './ShiftTime';

describe('class ShiftTime', () => {
  test('it should valid params constructor', () => {
    new ShiftTime({
      hours: 11,
      minutes: 59,
    });

    new ShiftTime([11, 59]);
  });

  test('it should check hours out of range', () => {
    expect(() => {
      new ShiftTime([25, 59]);
    }).toThrow(HourOutOfRangeError);

    expect(() => {
      new ShiftTime([-1, 59]);
    }).toThrow(HourOutOfRangeError);

    expect(() => {
      new ShiftTime([-1, -1]);
    }).toThrow(HourOutOfRangeError);
  });

  test('it should check minutes out of range', () => {
    expect(() => {
      new ShiftTime([11, 60]);
    }).toThrow(MinuteOutOfRangeError);

    expect(() => {
      new ShiftTime([11, -1]);
    }).toThrow(MinuteOutOfRangeError);
  });

  test('it should to string correct', () => {
    expect(new ShiftTime([11, 59]).toString()).toEqual('11:59');
  });

  test('it equals operator', () => {
    expect(new ShiftTime([11, 59]).equals(new ShiftTime([11, 59]))).toBe(true);

    expect(new ShiftTime([11, 59]).equals(new ShiftTime([11, 58]))).toBe(false);
  });

  test('it greater operator', () => {
    expect(new ShiftTime([11, 59]) > new ShiftTime([11, 58])).toBe(true);

    expect(new ShiftTime([11, 59]) > new ShiftTime([12, 58])).toBe(false);
  });

  test('it lesser operator', () => {
    expect(new ShiftTime([11, 59]) < new ShiftTime([12, 58])).toBe(true);

    expect(new ShiftTime([11, 59]) < new ShiftTime([11, 58])).toBe(false);
  });

  test('it greater or equal operator', () => {
    expect(new ShiftTime([11, 59]) >= new ShiftTime([11, 58])).toBe(true);

    expect(new ShiftTime([11, 59]) >= new ShiftTime([11, 59])).toBe(true);

    expect(new ShiftTime([11, 59]) >= new ShiftTime([12, 59])).toBe(false);
  });

  test('it lesser or equal operator', () => {
    expect(new ShiftTime([11, 59]) <= new ShiftTime([12, 0])).toBe(true);

    expect(new ShiftTime([11, 59]) <= new ShiftTime([11, 59])).toBe(true);

    expect(new ShiftTime([11, 59]) <= new ShiftTime([11, 58])).toBe(false);
  });
});
