import Shift, { ShiftTimeValidationError } from './Shift';

describe('class Shift', () => {
  test('it should valid params constructor', () => {
    new Shift({
      startTime: {
        hours: 11,
        minutes: 59,
      },
      endTime: {
        hours: 12,
        minutes: 59,
      },
    });

    new Shift([
      [11, 59],
      [12, 59],
    ]);
  });

  test('it startTime should be less or equal than endTime', () => {
    expect(() => {
      new Shift([
        [11, 59],
        [11, 0],
      ]);
    }).toThrow(ShiftTimeValidationError);
  });

  test('it correct toString()', () => {
    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).toString()
    ).toBe('11:59-12:30');
  });

  test('it equals', () => {
    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).equals(
        new Shift([
          [11, 59],
          [12, 30],
        ])
      )
    ).toBe(true);

    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).equals(
        new Shift([
          [11, 59],
          [12, 32],
        ])
      )
    ).toBe(false);
  });

  test('it intersect', () => {
    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).isIntersect(
        new Shift([
          [12, 30],
          [14, 30],
        ])
      )
    ).toBe(true);

    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).isIntersect(
        new Shift([
          [12, 0],
          [14, 30],
        ])
      )
    ).toBe(true);

    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).isIntersect(
        new Shift([
          [11, 0],
          [14, 30],
        ])
      )
    ).toBe(true);

    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).isIntersect(
        new Shift([
          [11, 0],
          [12, 10],
        ])
      )
    ).toBe(true);

    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).isIntersect(
        new Shift([
          [12, 31],
          [14, 30],
        ])
      )
    ).toBe(false);

    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).isIntersect(
        new Shift([
          [10, 30],
          [11, 58],
        ])
      )
    ).toBe(false);
  });

  test('it greater than', () => {
    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).isGreaterThan(
        new Shift([
          [10, 59],
          [11, 58],
        ])
      )
    ).toBe(true);

    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).isGreaterThan(
        new Shift([
          [10, 59],
          [11, 59],
        ])
      )
    ).toBe(false);

    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).isGreaterThan(
        new Shift([
          [10, 59],
          [12, 0],
        ])
      )
    ).toBe(false);
  });

  test('it less than', () => {
    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).isLessThan(
        new Shift([
          [12, 0],
          [14, 30],
        ])
      )
    ).toBe(false);

    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).isLessThan(
        new Shift([
          [12, 30],
          [14, 30],
        ])
      )
    ).toBe(false);

    expect(
      new Shift([
        [11, 59],
        [12, 30],
      ]).isLessThan(
        new Shift([
          [12, 31],
          [14, 30],
        ])
      )
    ).toBe(true);
  });
});
