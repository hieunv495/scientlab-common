import ShiftTime, { ShiftTimeArgs, ShiftTimeData } from './ShiftTime';

export interface ShiftData {
  startTime: ShiftTimeData;
  endTime: ShiftTimeData;
}

export class ShiftTimeValidationError extends Error {
  constructor({ startTime, endTime }: ShiftData) {
    super(
      `Start time ${startTime} should be less or equal to end time ${endTime} `
    );
    this.name = 'ShiftTimeValidationError';
    Object.setPrototypeOf(this, ShiftTimeValidationError.prototype);
  }
}

export type ShiftArgs = ShiftData | [ShiftTimeArgs, ShiftTimeArgs];

export default class Shift implements ShiftData {
  startTime: ShiftTime;
  endTime: ShiftTime;

  constructor(args: ShiftArgs) {
    if (args instanceof Array) {
      this.startTime = new ShiftTime(args[0]);
      this.endTime = new ShiftTime(args[1]);
    } else {
      this.startTime = new ShiftTime(args.startTime);
      this.endTime = new ShiftTime(args.endTime);
    }

    if (this.startTime > this.endTime) {
      throw new ShiftTimeValidationError({
        startTime: this.startTime,
        endTime: this.endTime,
      });
    }
  }

  toString(): string {
    return `${this.startTime}-${this.endTime}`;
  }

  equals(secondShift: Shift): boolean {
    return (
      this.startTime.equals(secondShift.startTime) &&
      this.endTime.equals(secondShift.endTime)
    );
  }

  isIntersect(secondShift: Shift): boolean {
    //   Start right
    if (secondShift.startTime > this.endTime) {
      return false;
    }
    // Start middle
    else if (
      secondShift.startTime <= this.endTime &&
      secondShift.startTime >= this.startTime
    ) {
      return true;
    }

    // Start left

    // End left
    else if (secondShift.endTime < this.startTime) {
      return false;
    }

    // End middle
    else if (
      secondShift.endTime <= this.endTime &&
      secondShift.endTime >= this.startTime
    ) {
      return true;
    }

    // End right
    else {
      return true;
    }
  }

  isGreaterThan(secondShift: Shift): boolean {
    console.log(this.startTime, this.endTime);
    return this.startTime > secondShift.endTime;
  }

  isLessThan(secondShift: Shift): boolean {
    return this.endTime < secondShift.startTime;
  }
}
