export interface ShiftTimeData {
  hours: number;
  minutes: number;
}

export class HourOutOfRangeError extends Error {
  constructor(hours: number) {
    super(`Hour ${hours} is out of range`);
    this.name = 'HourOutOfRange';

    Object.setPrototypeOf(this, HourOutOfRangeError.prototype);
  }
}

export class MinuteOutOfRangeError extends Error {
  constructor(minutes: number) {
    super(`Minutes ${minutes} is out of range`);
    this.name = 'MinuteOutOfRangeError';
    Object.setPrototypeOf(this, MinuteOutOfRangeError.prototype);
  }
}

const formattedNumber = (n: number, digits: number) =>
  n.toLocaleString('en-US', {
    minimumIntegerDigits: digits,
    useGrouping: false,
  });

export type ShiftTimeArgs = ShiftTimeData | [number, number];

export default class ShiftTime implements ShiftTimeData {
  hours: number;
  minutes: number;
  constructor(args: ShiftTimeArgs) {
    if (args instanceof Array) {
      this.hours = args[0];
      this.minutes = args[1];
    } else {
      this.hours = args.hours;
      this.minutes = args.minutes;
    }

    if (this.hours < 0 || this.hours > 24) {
      throw new HourOutOfRangeError(this.hours);
    } else if (this.minutes < 0 || this.minutes >= 60) {
      throw new MinuteOutOfRangeError(this.minutes);
    }
  }

  toString(): string {
    return `${this.hours}:${this.minutes}`;
  }

  valueOf(): string {
    return `${formattedNumber(this.hours, 2)}:${formattedNumber(
      this.minutes,
      2
    )}`;
  }

  equals(other: ShiftTime): boolean {
    return this.hours === other.hours && this.minutes === other.minutes;
  }
}
